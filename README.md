# CONFIGURATION MANAGEMENT

The purpose of Configuration Management (CM) is to establish and
maintain the integrity of work products using configuration identification,
configuration control, configuration status accounting, and configuration
audits.


## SG 2 Track and Control Changes

Changes to the work products under configuration management are tracked
and controlled.

The specific practices under this specific goal serve to maintain baselines after they are established by specific practices under the Establish Baselines specific goal.



### SP 2.2. Control Configuration Items 

Control changes to configuration items.


Control is maintained over the configuration of the work product baseline.
This control includes tracking the configuration of each configuration item,
approving a new configuration if necessary, and updating the baseline.

#### Example Work Products

1. [ ] Revision history of configuration items

   *Justification:*

1. [ ] Archives of baselines

   *Justification:*

#### Subpractices

1. [ ] Control changes to configuration items throughout the life of the product or service.

   *Justification:*


1. [ ]  Obtain appropriate authorization before changed configuration items are entered into the configuration management system.
     - For example, authorization can come from the CCB, the project manager, product owner, or the customer.
  
   *Justification:*


1. [ ] Check in and check out configuration items in the configuration management system for incorporation of changes in a manner that maintains the correctness and integrity of configuration items.
  
   Examples of check-in and check-out steps include the following:
    - Confirming that the revisions are authorized
    - Updating the configuration items
    - Archiving the replaced baseline and retrieving the new baseline
    - Commenting on the changes made to the item
    - Tying changes to related work products such as requirements, user stories, and tests

   *Justification:*

1. [ ] Perform reviews to ensure that changes have not caused unintended effects on the baselines (e.g., ensure that changes have not compromised the safety or security of the system).

   *Justification:*

1. [ ] Record changes to configuration items and reasons for changes as appropriate.
    - If a proposed change to the work product is accepted, a schedule is identified for incorporating the change into the work product and other affected areas.
    - Configuration control mechanisms can be tailored to categories of changes. For example, the approval considerations could be less stringent for component changes that do not affect other components.
    - Changed configuration items are released after review and approval of configuration changes. Changes are not official until they are released.

   *Justification:*